var map = L.map('map').setView([37.8, -96], 4);
console.log(map);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 18
}).addTo(map);

if (document.querySelector('.onoffswitch-checkbox').checked) {
    L.geoJson(statesData).addTo(map);
} else {
    L.geoJson(statesDataTwo).addTo(map);
}

function change()
{
    //console.log(map);

    //map.remove();
    if (document.querySelector('.onoffswitch-checkbox').checked) {
        var checked = "checked"
    } else {
        var checked = "unchecked"
    }
    var element = document.getElementById('map');
    element.parentNode.removeChild(element);
    mapDiv = document.createElement('div');
    mapDiv.id = "map";
    mapDiv.innerHTML = '<div class="onoffswitch"><input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" onclick="change();" ' + checked +'><label class="onoffswitch-label" for="myonoffswitch"><div class="onoffswitch-inner"></div><div class="onoffswitch-switch"></div></label>';
    document.body.appendChild(mapDiv);
    var elContainer = document.getElementById('container');
    elContainer.parentNode.removeChild(elContainer);
    chartDiv = document.createElement('div');
    chartDiv.id = "container";
    document.body.appendChild(chartDiv);
    var map = L.map('map').setView([37.8, -96], 4);

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
        maxZoom: 18
    }).addTo(map);

    if (document.querySelector('.onoffswitch-checkbox').checked) {
        L.geoJson(statesData).addTo(map);
    } else {
        L.geoJson(statesDataTwo).addTo(map);
    }

    function getColor(d) {
     return d > 0.008 ? '#FF0000 ' :
            d > 0.007  ? '#FF4400' :
            d > 0.006  ? '#FF8800' :
            d > 0.005  ? '#FFCC00' :
            d > 0.004   ? '#EEFF00' :
            d > 0.003   ? '#AAFF00' :
            d > 0.002  ? '#66FF00' :
                       '#22FF00 ';
    }
    function style(feature) {
        return {
            fillColor: getColor((feature.properties.total_murders/feature.properties.population)*100),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
        };
    }

    L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: mouseoutReset,
            click: zoomToFeature
        });
    }
    function highlightFeature(e) {
        var layer = e.target;
        layer.setStyle({
            weight: 5,
            color: '#6666BD',
            dashArray: '',
            fillOpacity: 0.7
        });
        info.update(layer.feature.properties);
        if (!L.Browser.ie && !L.Browser.opera) {
            layer.bringToFront();
        }

    }

    function mouseoutReset(e) {
        var layer = e.target;
        layer.setStyle({
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
        });
        info.update();
    }
    function zoomToFeature(e) {
        var layer = e.target;
        map.fitBounds(e.target.getBounds());

        $('#container').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Type of weapon'
            },
            xAxis: {
                categories: ['Handguns', 'Riffles', 'Shotguns', 'Firearms (type unknown)', 'Knives or cutting instruments', 'Other weapons', 'Hands, fists, feet']
                //categories: ['Handguns', 'Riffles']
            },
            yAxis: {
                title: {
                    text: 'Number of cases'
                }
            },
            series: [{
                //data: ["1, 20"]
                name: 'Year 2007',
                data: [e.target.feature.val.Handguns, e.target.feature.val.Riffles, e.target.feature.val.Shotguns, e.target.feature.val.Firearms, e.target.feature.val.Knives, e.target.feature.val.Other, e.target.feature.val.Hands]
            }, 
            {
                name: 'Year 2009',
                data: [e.target.feature.aldila.Handguns, e.target.feature.aldila.Riffles, e.target.feature.aldila.Shotguns, e.target.feature.aldila.Firearms, e.target.feature.aldila.Knives, e.target.feature.aldila.Other, e.target.feature.aldila.Hands]
             }]
        });
    }

    var legend = L.control({position: 'bottomright'});

    legend.onAdd = function (map) {

        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008],
            labels = [];
        div.innerHTML += '<h4>Total Murders(%)</h4>';
        // loop through our density intervals and generate a label with a colored square for each interval
        for (var i = 0; i < grades.length; i++) {
            div.innerHTML +=
                '<i style="background:' + getColor(grades[i] + 0.001) + '"></i> ' +
                grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
        }

        return div;
    };

    legend.addTo(map);
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        this.update();
        return this._div;
    };

    // method that we will use to update the control based on feature properties passed
    info.update = function (props) {
        this._div.innerHTML = '<h4>US Total Percentage of Murder Case in 2007</h4>' +  (props ?
            '<b>' + props.name + '</b><br />' +  ((props.total_murders/props.population)*100).toFixed(4)+ ' %. from all the population'+'<br />'+' Click for more info.'
            : 'Hover over a state');
    };

    info.addTo(map);

    

}
   
function getColor(d) {
     return d > 0.008 ? '#FF0000 ' :
            d > 0.007  ? '#FF4400' :
            d > 0.006  ? '#FF8800' :
            d > 0.005  ? '#FFCC00' :
            d > 0.004   ? '#EEFF00' :
            d > 0.003   ? '#AAFF00' :
            d > 0.002  ? '#66FF00' :
                       '#22FF00 ';
}
function style(feature) {
    return {
        fillColor: getColor((feature.properties.total_murders/feature.properties.population)*100),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: mouseoutReset,
        click: zoomToFeature
    });
}
function highlightFeature(e) {
    var layer = e.target;
    layer.setStyle({
        weight: 5,
        color: '#6666BD',
        dashArray: '',
        fillOpacity: 0.7
    });
    info.update(layer.feature.properties);
    if (!L.Browser.ie && !L.Browser.opera) {
        layer.bringToFront();
    }

}

function mouseoutReset(e) {
	var layer = e.target;
	layer.setStyle({
	    weight: 2,
	    opacity: 1,
	    color: 'white',
	    dashArray: '3',
	    fillOpacity: 0.7
    });
    info.update();
}
function zoomToFeature(e) {
    var layer = e.target;
    map.fitBounds(e.target.getBounds());

    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Type of weapon'
        },
        xAxis: {
            categories: ['Handguns', 'Riffles', 'Shotguns', 'Firearms (type unknown)', 'Knives or cutting instruments', 'Other weapons', 'Hands, fists, feet']
            //categories: ['Handguns', 'Riffles']
        },
        yAxis: {
            title: {
                text: 'Number of cases'
            }
        },
        series: [{
            //data: ["1, 20"]
            name: 'Year 2007',
            data: [e.target.feature.val.Handguns, e.target.feature.val.Riffles, e.target.feature.val.Shotguns, e.target.feature.val.Firearms, e.target.feature.val.Knives, e.target.feature.val.Other, e.target.feature.val.Hands]
        }, 
        {
            name: 'Year 2009',
            data: [e.target.feature.aldila.Handguns, e.target.feature.aldila.Riffles, e.target.feature.aldila.Shotguns, e.target.feature.aldila.Firearms, e.target.feature.aldila.Knives, e.target.feature.aldila.Other, e.target.feature.aldila.Hands]
         }]
    });
}

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = [0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008],
        labels = [];
    div.innerHTML += '<h4>Total Murders(%)</h4>';
    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' + getColor(grades[i] + 0.001) + '"></i> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
    }

    return div;
};

legend.addTo(map);
var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
    this._div.innerHTML = '<h4>US Total Percentage of Murder Case in 2007</h4>' +  (props ?
        '<b>' + props.name + '</b><br />' +  ((props.total_murders/props.population)*100).toFixed(4)+ ' %. from all the population'+'<br />'+' Click for more info.'
        : 'Hover over a state');
};

info.addTo(map);

